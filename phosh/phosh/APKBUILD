# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=phosh
pkgver=0.0.3_git20190716
pkgrel=0
_commit="9a0ab4df8b1a812d52e0d270adf7808c38449a90"
_gvc_commit="ae1a34aafce7026b8c0f65a43c9192d756fe1057"
pkgdesc="A shell PoC for the Librem5"
arch="x86 x86_64 aarch64"
url="https://source.puri.sm/Librem5/phosh"
license="GPL-3.0-only"
depends="gtk+3.0 wayland-protocols phoc gnome-session bash dbus-x11 gnome-settings-daemon virtboard libpulse"
makedepends="gtk+3.0-dev meson ninja gnome-desktop-dev libhandy-dev gcr-dev upower-dev linux-pam-dev git cmake pulseaudio-dev networkmanager-dev polkit-elogind-dev"
subpackages="$pkgname-lang"
source="
	$pkgname-$_commit.tar.gz::https://source.puri.sm/Librem5/$pkgname/-/archive/$_commit.tar.gz
	libgvc-$_gvc_commit.tar.gz::https://gitlab.gnome.org/GNOME/libgnome-volume-control/-/archive/$_gvc_commit/libgnome-volume-control-$_gvc_commit.tar.gz
	stop-hardcoding-resolution.patch
	phosh.desktop"
options="!check" # Needs a running Wayland compositor
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	cd "$srcdir"
	mv libgnome-volume-control*/* "$builddir"/subprojects/gvc/
}

build() {
	meson . _build --prefix=/usr
	ninja -C _build
}

check() {
	ninja -C _build test
}

package() {
	DESTDIR="$pkgdir/" ninja -C _build install

	install -D -m644 "$srcdir"/phosh.desktop \
		"$pkgdir"/usr/share/wayland-sessions/phosh.desktop

}
sha512sums="946df091c9053dc32412f9b8f293d8378e734c023d900e18cfc2931991500bdad52feaee199b495d840d765e9b70d36bf6825750b26c2c480306a6be02517040  phosh-9a0ab4df8b1a812d52e0d270adf7808c38449a90.tar.gz
723334bff55927363dab47ef22c71dcaf94263fe76e49c40f1cbfbd5f86383e68fd4bf2182eb5777dda8e2ede4ee4710e1a7ab1379d3ca40d68f68ff30c62e21  libgvc-ae1a34aafce7026b8c0f65a43c9192d756fe1057.tar.gz
93c05f86a314bbd466b083cd9dfe0ba40803020d4865d666f2f67356be41f1769116f34eaade66d19a0f0cec2d79941986fef24a9a2fa19481c769019aef9ba3  stop-hardcoding-resolution.patch
6644870edbbbc6b88d6e19f7771d81dba1a11066c2b34e4c22736db73a2dfd0d4909b4967503059c35385c5139a834a5c06a3c56b148ba1275d7f089c0c5f33c  phosh.desktop"
